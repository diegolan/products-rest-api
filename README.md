### How to compile and run the application with an example for each call? ###

You can use this maven command:

**mvn spring-boot:run**

Calls can be made by any browser. Try these URLs:

* http://localhost:8080/product/list
* http://localhost:8080/product/list?id=1
* http://localhost:8080/product/list/with?option=parent
* http://localhost:8080/product/list/with?option=images
* http://localhost:8080/product/list/with?option=parent&option=images
* http://localhost:8080/product/list/with?option=parent&id=2
* http://localhost:8080/product/list/with?option=images&id=1
* http://localhost:8080/product/list/with?option=parent&option=images&id=3
* http://localhost:8080/product/1/images
* http://localhost:8080/product/1/children

TIP: you can switch the id parameter accordingly with your desire, using 1, 2 or 3.

**OBS: The default result is in XML. If you want to have JSON, you must have the header Accept application/json in your requests.**