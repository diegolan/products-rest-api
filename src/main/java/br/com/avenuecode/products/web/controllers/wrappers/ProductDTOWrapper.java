package br.com.avenuecode.products.web.controllers.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.avenuecode.products.business.services.dto.ProductDTO;

@XmlRootElement(name = "document", namespace = "br.com.avenuecode")
public class ProductDTOWrapper {

	@XmlElementWrapper(name = "products")
	@XmlElement(name = "product")
	private List<ProductDTO> products;

	public ProductDTOWrapper(List<ProductDTO> products) {
		this.products = products;
	}

	public List<ProductDTO> getProducts() {
		return products;
	}
	
	ProductDTOWrapper() {
		// needed for JAXB
	}

}
