package br.com.avenuecode.products.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.avenuecode.products.business.services.ProductService;

@Controller
public class MVCTestController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("products", productService.listAll(null));
		return "teste";
	}
	
	@RequestMapping("/pagina")
	public String pagina() {
		return "teste/pagina";
	}

}
