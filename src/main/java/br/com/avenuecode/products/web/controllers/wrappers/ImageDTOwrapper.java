package br.com.avenuecode.products.web.controllers.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.avenuecode.products.business.services.dto.ImageDTO;

@XmlRootElement(name = "document", namespace = "br.com.avenuecode")
public class ImageDTOwrapper {
	
	@XmlElementWrapper(name = "images")
	@XmlElement(name = "image")
	private List<ImageDTO> images;
	
	public ImageDTOwrapper(List<ImageDTO> images) {
		this.images = images;
	}

	public List<ImageDTO> getImages() {
		return images;
	}
	
	ImageDTOwrapper() {
		// needed for JAXB
	}

}
