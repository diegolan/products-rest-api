package br.com.avenuecode.products.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.avenuecode.products.business.services.ProductService;
import br.com.avenuecode.products.web.controllers.wrappers.ImageDTOwrapper;
import br.com.avenuecode.products.web.controllers.wrappers.ProductDTOWrapper;

@RestController
@RequestMapping(value = "/product", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
public class ProductRestController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/list")
	public ProductDTOWrapper listExcluding(@RequestParam(name = "id", required = false) Long id) {
		return new ProductDTOWrapper(productService.listAll(id));
	}
	
	@RequestMapping("/list/with")
	public ProductDTOWrapper listIncluding(@RequestParam(name = "id", required = false) Long id, @RequestParam(name = "option") List<String> options) {
		return new ProductDTOWrapper(productService.listAllWith(id, options));
	}
	
	@RequestMapping("/{id}/images")
	public ImageDTOwrapper findImageSet(@PathVariable("id") Long id) {
		return new ImageDTOwrapper(productService.listProductImages(id));
	}
	
	@RequestMapping("/{id}/children")
	public ProductDTOWrapper findProductSet(@PathVariable("id") Long id) {
		return new ProductDTOWrapper(productService.listProductChildren(id));
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> exceptionHandler(Exception e) {
		return new ResponseEntity<String>("You are using the API in a wrong way! Message: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	}
	
}