package br.com.avenuecode.products.aop.logging;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ServiceLogger {
	
	private static final String POINT_CUT = "execution(* br.com.avenuecode.products.business.services.*.*(..)) && target(target)";
	
	@Before(POINT_CUT)
	public void logBefore(JoinPoint joinPoint, Object target) {
		Logger log = getLog(target);
		log.debug("method: " + joinPoint.getSignature().getName());
		log.debug("arguments: " + Arrays.toString(joinPoint.getArgs()));
	}
	
	@AfterThrowing(value = POINT_CUT, throwing = "e")
	public void logAfterThrowing(JoinPoint joinPoint, Object target, Exception e) {
		Logger log = getLog(target);
		log.error("method: " + joinPoint.getSignature().getName());
		log.error("arguments: " + Arrays.toString(joinPoint.getArgs()));
		log.error("error trace", e);
	}
	
	private Logger getLog(Object target) {
		return LoggerFactory.getLogger(target.getClass());
	}

}
