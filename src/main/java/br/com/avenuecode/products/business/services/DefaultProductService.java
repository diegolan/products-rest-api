package br.com.avenuecode.products.business.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.avenuecode.products.business.repositories.ImageRepository;
import br.com.avenuecode.products.business.repositories.ProductRepository;
import br.com.avenuecode.products.business.services.assemblers.ImageAssembler;
import br.com.avenuecode.products.business.services.assemblers.ProductsWithRelationshipsAssembler;
import br.com.avenuecode.products.business.services.assemblers.ProductsWithoutRelationshipsAssembler;
import br.com.avenuecode.products.business.services.dto.ImageDTO;
import br.com.avenuecode.products.business.services.dto.ProductDTO;

@Service
public class DefaultProductService implements ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ImageRepository imageRepository;

	@Override
	public List<ProductDTO> listAll(Long id) {
		ProductsWithoutRelationshipsAssembler assembler = new ProductsWithoutRelationshipsAssembler();
		if (id == null) {
			return assembler.transformToDTO(productRepository.findAll());
		}
		return assembler.transformToDTO(productRepository.findAllById(id));
	}

	@Override
	public List<ProductDTO> listAllWith(Long id, List<String> options) {
		ProductsWithRelationshipsAssembler assembler = new ProductsWithRelationshipsAssembler(options);
		if (id == null) {
			return assembler.transformToDTO(productRepository.findAll());
		}
		return assembler.transformToDTO(productRepository.findAllById(id));
	}

	@Override
	public List<ImageDTO> listProductImages(Long id) {
		return new ImageAssembler().transformToDTO(imageRepository.findAllByProductId(id));
	}

	@Override
	public List<ProductDTO> listProductChildren(Long id) {
		return new ProductsWithoutRelationshipsAssembler().transformToDTO(productRepository.findAllByParentId(id));
	}

}
