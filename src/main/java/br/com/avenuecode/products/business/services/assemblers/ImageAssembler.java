package br.com.avenuecode.products.business.services.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

import br.com.avenuecode.products.business.models.Image;
import br.com.avenuecode.products.business.services.dto.ImageDTO;

public class ImageAssembler {
	
	public List<ImageDTO> transformToDTO(final Iterable<Image> images) {
		Validate.notNull(images, "images cannot be null.");
		
		List<ImageDTO> transformedImages = new ArrayList<ImageDTO>();
		
		for (Image image : images) {
			transformedImages.add(parseImage(image));
		}
		
		return transformedImages;
	}

	private ImageDTO parseImage(Image image) {
		return new ImageDTO(image.getId(), image.getType());
	}

}
