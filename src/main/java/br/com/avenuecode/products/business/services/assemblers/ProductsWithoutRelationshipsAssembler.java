package br.com.avenuecode.products.business.services.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

import br.com.avenuecode.products.business.models.Product;
import br.com.avenuecode.products.business.services.dto.ProductDTO;

public class ProductsWithoutRelationshipsAssembler {
	
	public List<ProductDTO> transformToDTO(final Iterable<Product> products) {
		Validate.notNull(products, "products cannot be null.");
		
		List<ProductDTO> transformedProducts = new ArrayList<ProductDTO>();
		
		for (Product product : products) {
			transformedProducts.add(parseProduct(product));
		}
		
		return transformedProducts;
	}

	private ProductDTO parseProduct(Product product) {
		return new ProductDTO(product.getId(), product.getName(), product.getDescription());
	}

}
