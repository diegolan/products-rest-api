package br.com.avenuecode.products.business.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.avenuecode.products.business.models.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {

	Set<Image> findAllByProductId(Long id);

}
