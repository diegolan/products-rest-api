package br.com.avenuecode.products.business.services.dto;

import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductDTO {

	@XmlElement
	private Long id;
	
	@XmlElement
	private String name;
	
	@XmlElement
	private String description;
	
	@XmlElementWrapper(name = "images")
	@XmlElement(name = "image")
	private List<ImageDTO> images;
	
	@XmlElement
	private ProductDTO parent;

	public ProductDTO(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public List<ImageDTO> getImages() {
		return images;
	}

	public synchronized void setImages(List<ImageDTO> images) {
		if (this.images == null) {
			this.images = Collections.unmodifiableList(images);
		}
	}

	public ProductDTO getParent() {
		return parent;
	}

	public synchronized void setParent(ProductDTO parent) {
		if (this.parent == null) {
			this.parent = parent;
		}
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	ProductDTO() {
		// needed for JAXB
	}

}
