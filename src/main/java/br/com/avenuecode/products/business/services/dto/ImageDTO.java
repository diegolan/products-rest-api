package br.com.avenuecode.products.business.services.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageDTO {
	
	@XmlElement
	private Long id;
	
	@XmlElement
	private String type;
	
	public ImageDTO(Long id, String type) {
		this.id = id;
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}

	public String getType() {
		return type;
	}
	
	ImageDTO() {
		// needed for JAXB
	}
	
}
