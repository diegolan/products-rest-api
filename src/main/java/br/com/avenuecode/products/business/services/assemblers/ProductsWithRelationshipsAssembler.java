package br.com.avenuecode.products.business.services.assemblers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.Validate;

import br.com.avenuecode.products.business.models.Image;
import br.com.avenuecode.products.business.models.Product;
import br.com.avenuecode.products.business.services.dto.ImageDTO;
import br.com.avenuecode.products.business.services.dto.ProductDTO;

public class ProductsWithRelationshipsAssembler {
	
	private final List<String> options;

	public ProductsWithRelationshipsAssembler(List<String> options) {
		Validate.notNull(options, "options cannot be null.");
		
		if (!options.contains("parent") && !options.contains("images")) {
			throw new RuntimeException("options must contains parent or images.");
		}
		
		this.options = options;
	}
	
	public List<ProductDTO> transformToDTO(final Iterable<Product> products) {
		Validate.notNull(products, "products cannot be null.");
		
		List<ProductDTO> transformedProducts = new ArrayList<ProductDTO>();
		
		for (Product product : products) {
			ProductDTO dto = parseProduct(product);
			if (options.contains("parent")) {
				dto.setParent(parseProduct(product.getParent()));
			}
			if (options.contains("images")) {
				dto.setImages(parseImages(product.getImages()));
			}
			transformedProducts.add(dto);
		}
		
		return transformedProducts;
	}
	
	private List<ImageDTO> parseImages(Set<Image> images) {
		return new ImageAssembler().transformToDTO(images);
	}

	private ProductDTO parseProduct(Product product) {
		if (product == null) {
			return null;
		}
		return new ProductDTO(product.getId(), product.getName(), product.getDescription());
	}

}
