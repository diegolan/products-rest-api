package br.com.avenuecode.products.business.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.avenuecode.products.business.models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Set<Product> findAllById(Long id);
	
	Product findParentById(Long id);

	Set<Product> findAllByParentId(Long id);

}
