package br.com.avenuecode.products.business.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private Set<Image> images = new HashSet<Image>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_product_id")
	private Product parent;
	
	public Product() {
	}
	
	public Product(Product product) {
		setId(product.getId());
		setName(product.getName());
		setDescription(product.getDescription());
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}
	
	public void setParent(Product parent) {
		this.parent = parent;
	}
	
	public Product getParent() {
		return parent;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof Product) {
			Product parsed = (Product) obj;
			return id.equals(parsed.id);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.valueOf(id);
	}

}
