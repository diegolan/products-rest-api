package br.com.avenuecode.products.business.services;

import java.util.List;

import br.com.avenuecode.products.business.services.dto.ImageDTO;
import br.com.avenuecode.products.business.services.dto.ProductDTO;

public interface ProductService {
	
	List<ProductDTO> listAll(Long id);
	
	List<ProductDTO> listAllWith(Long id, List<String> options);

	List<ImageDTO> listProductImages(Long id);

	List<ProductDTO> listProductChildren(Long id);

}
