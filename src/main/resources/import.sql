insert into product (id, description, name, parent_product_id) values (0, 'some description about product zero', 'zero', null);
insert into product (id, description, name, parent_product_id) values (1, 'some description about product one', 'one', 0);
insert into product (id, description, name, parent_product_id) values (2, 'some description about product two', 'two', 1);
insert into product (id, description, name, parent_product_id) values (3, 'some description about product three', 'three', 1);
insert into image (id, type, product_id) values (1, 'jpg', 1);
insert into image (id, type, product_id) values (2, 'png', 2);
insert into image (id, type, product_id) values (3, 'gif', 2);
insert into image (id, type, product_id) values (4, 'bmp', 3);